#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <fcntl.h>
#include <elf.h>
#include <unistd.h>
#include <string.h>
#include <inttypes.h>
#include <sys/wait.h>
#include <sys/procfs.h>
#include <sys/user.h>
#include <sys/ptrace.h>
#include <asm/ldt.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <ucontext.h>
#include <sys/syscall.h>
#include "err.h"

#define false 0
#define true 1
#define bool short

#define NEW_STACK_LOC  (0x00040000)
#define NEW_STACK_SIZE (0x20000)
#define MEMO 1000

int getProt(Elf32_Word flags)
{
    int res;
    res = PROT_NONE;
    if ((flags & PF_X) != 0)
        res = res | PROT_EXEC;
    if ((flags & PF_R) != 0)
        res = res | PROT_READ;
    if ((flags & PF_W) != 0)
        res = res | PROT_WRITE;
    
    return res;
}

struct elf_prstatus* readRegs(int elfFd, Elf32_Ehdr* hdr)
{
    struct elf_prstatus* status; 
    Elf32_Phdr* phdr;
    char *p = NULL;
    int size;
    int i;
    Elf32_Nhdr *note;
    int mapped[MEMO];
    int mappedIter;
    mappedIter = 0;

    for (i = 0; i < MEMO; i++)
        mapped[i] = -1;

    if (lseek(elfFd, hdr->e_phoff, SEEK_SET) == -1)
        syserr("lseek failed");
    
    size = hdr->e_phentsize * hdr->e_phnum;
    p = sbrk(size);
    if (p == (void *) -1)
        syserr("sbrk failed");
    if (read(elfFd, p, size) == -1)
        syserr("read failed");

    phdr = (Elf32_Phdr *) p;
    for (i = 0; i < hdr->e_phnum; i++)
    {
        if (phdr->p_type == PT_LOAD)
        {
            int fd;
            //fd = fileno(elf);
            fd = elfFd;
            //if (fd == -1)
            //    syserr("fileno failed");
            int prot;
            prot = getProt(phdr->p_flags); 
            void* start;
            start = (void *)phdr->p_vaddr;
            void* ret;
            bool mappedBefore;
            mappedBefore = false;
            int ptIter;
            ptIter = 0;

            for (ptIter = 0; ptIter < mappedIter; ptIter += 2)
            {
                if (phdr->p_vaddr == mapped[ptIter] && phdr->p_vaddr + phdr->p_memsz == mapped[ptIter + 1])
                    mappedBefore = true;
            }

            //if memory was mapped before (in NT_FILE section) we don't have to allocate memory for it
            if (mappedBefore == false)
            { 
                if (mmap(start, phdr->p_memsz, PROT_WRITE | PROT_EXEC, MAP_PRIVATE | MAP_FIXED | MAP_ANONYMOUS, -1, 0) == MAP_FAILED)
                    syserr("mmap failed");
            }

            //don't call mmap if phdr->p_filesz is 0 (it causes SIGSEGV)
            if (phdr->p_filesz != 0)
            {
                //allocate to arbitrary address
                if ((ret = mmap((void *) 0x2048000, phdr->p_filesz, PROT_READ | PROT_WRITE | PROT_EXEC, MAP_PRIVATE | MAP_FIXED, fd, phdr->p_offset)) == MAP_FAILED)
                    syserr("mmap failed");

                //move where it is supposed to be
                memcpy(start, ret, phdr->p_filesz);

                //no longer need it
                if (munmap(ret, phdr->p_filesz) == -1)
                    syserr("munmap failed");
            }

            if (mprotect(start, phdr->p_memsz, prot) == -1)
                syserr("mprotect failed");
        }
             
        if (phdr->p_type == PT_NOTE)
        {
            unsigned char* pdata;
            int padding, padding2;
            Elf32_Word readAllr = 0;
            pdata = sbrk(phdr->p_filesz);
            if (pdata == (void *) -1)
                syserr("sbrk failed");

            if (lseek(elfFd, phdr->p_offset, SEEK_SET) == -1)
                syserr("lseek failed");

            if (read(elfFd, pdata, phdr->p_filesz) == -1)
                syserr("read failed");
            note = (Elf32_Nhdr *) pdata;
            

            while (readAllr <= phdr->p_filesz) 
            {
                padding = (4 - (note->n_namesz % 4) % 4);
                padding2 = (4 - ((note->n_descsz + note->n_namesz) % 4) % 4);
                int paddingN, paddingD;
                paddingN = (4 - (note->n_namesz % 4)) % 4;
                paddingD = (4 - (note->n_descsz % 4)) % 4;

                if (note->n_type == NT_PRSTATUS)
                    status = (struct elf_prstatus *)(pdata + note->n_namesz + sizeof(*note) + paddingN);
                
                if (note->n_type == NT_386_TLS)
                {
                    int hm, iter;
                    hm = note->n_descsz / sizeof(struct user_desc);

                    for (iter = 0; iter < hm; iter++)
                    {
                        struct user_desc * udesPtr;
                        struct user_desc udes;
                        udesPtr = (void *)pdata + sizeof(Elf32_Nhdr) + (sizeof(struct user_desc) * iter) + note->n_namesz + padding;
                        udes = *udesPtr;
                        if (syscall(SYS_set_thread_area, &udes) == -1)
                            syserr("set_thread_area failed");
                    }
                }

                if (note->n_type == NT_FILE)
                {
                    long pageSize;
                    pageSize = sysconf(_SC_PAGE_SIZE);
                    if (pageSize == -1)
                        syserr("sysconf failed");
                    int32_t noel, startF, endF, offF;

                    noel = *(pdata + 20); //  number of mapped files
                    int iter;
                    volatile int nameOff;
                    iter = 0;
                    nameOff = 0;
                    while (iter < noel)
                    {
                        void* startMapPtr;
                        int32_t * ptrer;
                        ptrer = (void *) pdata + (4 * (7 + 3 * iter)); //start of mapping
                        startF = *ptrer;
                        startMapPtr = ptrer;
                        ptrer = (void *) pdata + (4 * (8 + 3 * iter)); //end of mapping
                        endF = *ptrer;
                        ptrer = (void *) pdata + (4 * (9 + 3 * iter)); //offset (in pages!)
                        offF = *ptrer; 
                        if (mappedIter >= MEMO - 2)
                            fatal("too many mappings!");
                        mapped[mappedIter] = startF;
                        mapped[mappedIter + 1] = endF;
                        mappedIter = mappedIter + 2;
                        
                        int mappedFileFd;
                        mappedFileFd = open(pdata + ((4 * (9 + 3 * (noel - 1)) + 4)) + nameOff, O_RDONLY);
                        if (mappedFileFd == -1)
                            syserr("open failed");
                        if (mmap((void *) startF, endF - startF, 
                                    PROT_READ | PROT_WRITE | PROT_EXEC, 
                                    MAP_PRIVATE | MAP_FIXED, mappedFileFd, 
                                    offF * pageSize) == MAP_FAILED)
                            syserr("mmap failed");

                        if (close(mappedFileFd) == -1)
                            syserr("close failed");

                        //calculate offset for names. remember about +1 for NULL terminator
                        nameOff = nameOff + strlen(pdata + (4 * (9 + 3 * (noel - 1)) + 4) + nameOff) + 1;
 
                        iter += 1;
                    }
                }

                //move our pointers - we did some reading
                pdata = pdata + note->n_descsz + paddingD + sizeof(Elf32_Nhdr) + note->n_namesz + paddingN;
                note = (Elf32_Nhdr *) pdata;
                readAllr = readAllr + note->n_descsz + paddingD + sizeof(Elf32_Nhdr) + note->n_namesz + paddingN;
            }
        }
        phdr++; 
    }
    return status;
}

inline void restoreRegs(struct user_regs_struct* regs)
{
    int a, b, c, d, ip, si, di, sp, bp, efl;
    a = regs->eax;
    b = regs->ebx;
    c = regs->ecx;
    d = regs->edx;
    sp = regs->esp;
    ip = regs->eip;
    si = regs->esi;
    di = regs->edi;
    bp = regs->ebp;
    efl = regs->eflags;

    register int r_a asm("eax") = a;
    register int r_b asm("ebx") = b;
    register int r_c asm("ecx") = c;
    register int r_d asm("edx") = d;

    register int r_si asm("esi") = si;
    register int r_di asm("edi") = di;
    register int r_sp asm("esp") = sp; 

    asm volatile("push %k9; push %k8; push %k7; popfl; pop %%ebp; ret" : : "r"(r_b), "r"(r_c), "r"(r_d), "r"(r_a), "r"(r_si), "r"(r_di), "r"(r_sp), "g"(efl), "g"(bp), "g"(ip) : );

}

struct user_regs_struct* prstatusToRegs(struct elf_prstatus * status)
{
    //register int foo asm("register_name"); -- foo always has value of register_name

    //copy everything, then use what is needed

    struct user_regs_struct* regs = sbrk(sizeof (struct user_regs_struct));
    if (regs == (void *)-1)
        syserr("sbrk failed");
    regs->ebx = status->pr_reg[0];
    regs->ecx = status->pr_reg[1];
    regs->edx = status->pr_reg[2];
    regs->esi = status->pr_reg[3];
    regs->edi = status->pr_reg[4];
    regs->ebp = status->pr_reg[5];
    regs->eax = status->pr_reg[6];
    regs->esp = status->pr_reg[15];
    regs->eip = status->pr_reg[12];
    regs->eflags = status->pr_reg[14];
    regs->orig_eax = status->pr_reg[11];
 
    regs->xds = status->pr_reg[7];
    regs->xes = status->pr_reg[8];
    regs->xfs = status->pr_reg[9];
    regs->xgs = status->pr_reg[10];
    regs->xcs = status->pr_reg[13];
    regs->xss = status->pr_reg[16];

    return regs;
}

bool elfCheckFile(Elf32_Ehdr *hdr)
{
    //check if it is a valid elf core
    if (!hdr)
        return false;

    if (hdr -> e_ident[EI_MAG0] != ELFMAG0 || 
            hdr -> e_ident[EI_MAG1] != ELFMAG1 ||
            hdr -> e_ident[EI_MAG2] != ELFMAG2 ||
            hdr -> e_ident[EI_MAG3] != ELFMAG3)
        fatal("wrong magic");

    if (hdr->e_type != ET_CORE)
        fatal("elf must be a core file");

    if (hdr->e_machine != EM_386)
        fatal("wrong architecture");

    if (hdr->e_version != EV_CURRENT)
        fatal("wrong elf version");

    return true;
}

Elf32_Ehdr* loadElf(int elfFd)
{
    Elf32_Ehdr* hdr = sbrk(sizeof(Elf32_Ehdr));
    if (hdr == (void *) -1)
        syserr("sbrk failed");
    if (read(elfFd, hdr, sizeof(Elf32_Ehdr)) == -1)
        fatal("read failed"); 
    if (!elfCheckFile(hdr))
        return NULL;
    else
        return hdr; 
}

static void smain(char* path)
{
    //actuall main
    //allready in "lower memory"
    FILE* elf = NULL;
    Elf32_Ehdr* hdr;
    struct elf_prstatus* status;
    struct user_regs_struct* regs;
    int elfFd;

    elfFd = open(path, O_RDONLY);
    if (elfFd == -1)
        syserr("open failed");

    hdr = loadElf(elfFd);
    if (hdr == NULL)
        fatal("failed to load elf");

    status = readRegs(elfFd, hdr); //and map all memory
    regs = prstatusToRegs(status);
    if (close(elfFd) == -1)
        syserr("close failed");
    restoreRegs(regs);
}

int main(int argc, char* argv[])
{
    if (argc != 2)
        fatal("Usage: ./resurect <core_file>");

    //main to move stack lower
    ucontext_t con;
    void * ret;
    
    //mmap so we have our deisred location
    if ((ret = mmap((void *) NEW_STACK_LOC, NEW_STACK_SIZE, PROT_WRITE | PROT_EXEC | PROT_READ, MAP_ANONYMOUS | MAP_PRIVATE | MAP_FIXED, -1, 0)) == MAP_FAILED)
        syserr("mmap failed");
   
    if (getcontext(&con) != 0)
        syserr("getcontext failed");

    con.uc_stack.ss_sp = ret;
    con.uc_stack.ss_size = NEW_STACK_SIZE;
    con.uc_link = NULL;

    makecontext(&con, (void *) (&smain), 1, argv[1]);
    if (setcontext(&con) == -1)
        syserr("swapcontext failed");
    return -1; //we won't get here
}
