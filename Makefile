TARGET = raise
FILES = czytanie.c err.c

CC = gcc
CFLAGS = -m32 -static -Wl,-Ttext-segment=0x4048000

ALL:
	$(CC) $(CFLAGS) $(FILES) -o $(TARGET)

clean:
	rm -rf raise
